import { Pain } from "./Metier/pain";
import { Kebab } from "./Metier/kebab";
import { Ingredient } from "./Metier/ingredient";
import { Sauce } from "./Metier/sauce";
import { Fromage } from "./Metier/fromage";

const prompts = require('prompts');

const whileLoop = async () => {
  console.log('---------------------------');
  
  let finKebab: boolean = false;
  const ingredients = [];
  let sauce = new Sauce('mayonaise');
  let fromages = [];

  while (finKebab == false) {
    const response = await prompts({
      type: 'text',
      name: 'value',
      message: 'Taper 1 pour ajouter un ingredient\n Taper 2 pour ajouter du fromage\n Taper 3 pour ajouter de la sauce\n Taper 4 pour passer la commande',
      validate: (value: number) => value != 1 && value != 2 && value != 3 && value != 4 ? 'Rentrer un chiffre correct' : true
    });

    if (response.value == 1) {

      const responseIngr = await prompts({
        type: 'text',
        name: 'value',
        message: 'Ajouter un ingredient',
        validate: (value: string) => value != "viande" && value != "poisson" && value != "salade" && value != "oignon" && value != "crevette" ? `L'ingredient demandé n'existe pas` : true
      });
      let ing = new Ingredient(responseIngr.value);
      ingredients.push(ing);
    } 
    if (response.value == 2) {

      const responseFromage = await prompts({
        type: 'text',
        name: 'value',
        message: 'Ajouter du fromage',
        validate: (value: string) => value != "cheddar" && value != "raclette" && value != "maroilles"  ? `Le fromage demandé n'existe pas` : true
      });
      let fromage = new Fromage(responseFromage.value);
      fromages.push(fromage);
    } 
    if (response.value == 3) {

      const responseSauce = await prompts({
        type: 'text',
        name: 'value',
        message: 'Ajouter une sauce',
        validate: (value: string) => value != "blanche" && value != "béchamel" && value != "algérienne" && value != "ketchup" && value != "crevette" ? `L'ingredient demandé n'existe pas` : true
      });
      sauce = new Sauce(responseSauce.value);
    } 
    else {
      finKebab = true;
      const pain = new Pain("briocher");
      let kebab = new Kebab(ingredients, pain, sauce, fromages);
      console.log(kebab.checkIfVegetarien() ? "Ce kebab est végétarien" : "Ce kebab n'est pas végétarien");
      console.log(kebab.checkIfPesetarien() ? "Ce kebab est pesitarien" : "Ce kebab n'est pas pesitarien");
      console.log(kebab);
    }
  }
}


whileLoop();