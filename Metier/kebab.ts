import { Ingredient } from './ingredient';
import { Pain } from './pain';
import { Sauce } from './sauce';
import { Fromage } from './fromage';

export class Kebab {
    ingredients: Ingredient[];
    fromages: Fromage[];
    pain: Pain;
    sauce: Sauce;

    constructor(ingredients: Ingredient[], pain: Pain, sauce: Sauce, fromages: Fromage[]) {
        this.ingredients = ingredients;
        this.pain = pain;
        this.sauce = sauce;
        this.fromages = fromages;
    }

    public checkIfVegetarien() {
        let isVegetarien = true;
        this.ingredients.forEach(ingredient => {
            if (ingredient.nom == "viande") {
                isVegetarien = false;
            }
        });

        return isVegetarien;
    }

    public checkIfPesetarien() {
        let isPesitarien = false;
        let isVegetarien: boolean = this.checkIfVegetarien();

        if (isVegetarien) {
            this.ingredients.forEach(ingredient => {
                console.log(ingredient.nom);

                if (ingredient.nom == "poisson" || ingredient.nom == "crevette") {
                    isPesitarien = true
                }
            });
        }

        return isPesitarien;
    }
}